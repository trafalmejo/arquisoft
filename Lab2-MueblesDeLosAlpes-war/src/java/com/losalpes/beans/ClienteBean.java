/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.losalpes.beans;

import clientes.Cliente;
import com.losalpes.servicios.IServicioCatalogoClientes;
import com.losalpes.servicios.ServicioCatalogoClientes;

/**
 *
 * @author Estudiante
 */
public class ClienteBean {

   
    //-----------------------------------------------------------
    // Atributos
    //-----------------------------------------------------------

    /**
     * Representa un nuevo mueble a ingresar
     */
    private Cliente cliente;

    /**
     * Relación con la interfaz que provee los servicios necesarios del catálogo.
     */
    private IServicioCatalogoClientes catalogo;

    //-----------------------------------------------------------
    // Constructor
    //-----------------------------------------------------------

    /**
     * Constructor de la clase principal
     */
    public ClienteBean()
    {
        cliente = new Cliente();
        catalogo = new ServicioCatalogoClientes();
    }

      //-----------------------------------------------------------
    // Getters y setters
    //-----------------------------------------------------------

    /**
     * Devuelve el objeto mueble
     * @return mueble Objeto mueble
     */
    public Cliente getCliente()
    {
        return cliente;
    }

    /**
     * Modifica el objeto mueble
     * @param mueble Nuevo mueble
     */
    public void setCliente(Cliente cliente)
    {
        this.cliente = cliente;
    }
    
       //-----------------------------------------------------------
    // Métodos
    //-----------------------------------------------------------

    /**
     * Agrega un nuevo mueble al sistema
     */
    public void agregarCliente()
    {
        catalogo.agregarCliente(cliente);
        cliente=new Cliente();
    }
    
     public void eliminarCliente(Cliente mu)
    {
        cliente=new Cliente();

    }
       public void editarCliente(Cliente mu)
    {
        cliente = new Cliente();
    }

    /**
     * Elimina la información del mueble
     */
    public void limpiar()
    {
        cliente=new Cliente();
    }
}
