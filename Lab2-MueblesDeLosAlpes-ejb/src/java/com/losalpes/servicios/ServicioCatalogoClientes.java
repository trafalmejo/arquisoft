/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$ ServicioCatalogoMock.java
 * Universidad de los Andes (Bogotá - Colombia)
 * Departamento de Ingeniería de Sistemas y Computación
 * Licenciado bajo el esquema Academic Free License version 3.0
 *
 * Ejercicio: Muebles de los Alpes
 * Autor: Juan Sebastián Urrego
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package com.losalpes.servicios;

import clientes.Cliente;
import java.util.ArrayList;
import java.util.List;


/**
 * Implementacion de los servicios del catalogo de clientes que se le prestan al sistema. [Mock Object]
 * @author Juan Sebastián Urrego
 */

public class ServicioCatalogoClientes implements IServicioCatalogoClientes
{

    //-----------------------------------------------------------
    // Atributos
    //-----------------------------------------------------------

    /**
     * Arreglo con los clientes del sistema
     */
    private ArrayList<Cliente> clientes;

    //-----------------------------------------------------------
    // Constructor
    //-----------------------------------------------------------

    /**
     * Constructor sin argumentos de la clase
     */
    public ServicioCatalogoClientes()
    {

        //Inicializa el arreglo de los clientes
        clientes=new ArrayList<Cliente>();
        Cliente c = new Cliente ("Daniel", "Castano", "Cedula", 12345678, 4111399, "Cr acasito"); 
        clientes.add(c);
        //Agrega los clientes del sistema
        
    }

    //-----------------------------------------------------------
    // Métodos
    //-----------------------------------------------------------

    /**
     * Agrega un cliente al sistema
     * @param cliente Nuevo cliente
     */
    @Override
    public void agregarCliente(Cliente cliente)
    {
        boolean existe = false;
        int posicion = 0 ;
        for(int i = 0 ; i < clientes.size() && existe != true ; i ++){
            Cliente comparo = clientes.get(i);
            if(comparo == cliente){
                existe = true;
                posicion = i;
            }
        }
        if (!existe){
        clientes.add(cliente);
        }
        else{
        clientes.set(posicion, cliente);   
        }
      }
    
    @Override
    public void eliminarCliente(Cliente cliente)
    {
        clientes.remove(cliente);
    }

     public void editarCliente(Cliente cliente)
    {
       
    }
    /**
     * Devuelve los clientes del sistema
     * @return clientes Arreglo con todos los clientes del sistema
     */
    @Override
    public List<Cliente> darClientes()
    {
        return clientes;
    }

}
