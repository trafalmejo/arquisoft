/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clientes;

/**
 *
 * @author Estudiante
 */
public class Cliente {
    
    private String nombres;
    private String apellidos;
    private String tipoId;
    private long documento;
    private long telefono;            
    private String direccion;
       /**
     * Indica si el mueble fue seleccionado
     */
    private boolean seleccion;
         
    public Cliente(){
        
    }
    public Cliente(String nom, String apell, String tipo, long doc, long tel, String dir){
nombres = nom;
apellidos = apell;
tipoId = tipo;
documento = doc;
telefono = tel;
direccion = dir;
        
    }
    
     /**
     * Devuelve el estado de selección del mueble
     * @return seleccion Verdadero o falso
     */
    public boolean isSeleccion()
    {
        return seleccion;
    }
     /**
     * Cambia el estado de selección de un mueble
     * @param seleccion Nuevo estado de selección
     */
    public void setSeleccion(boolean seleccion)
    {
        this.seleccion = seleccion;
    }
}
